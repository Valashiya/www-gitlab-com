---
layout: markdown_page
title: "Category Vision - Feature Flags"
---

- TOC
{:toc}

## Feature Flags

Feature flags unlock faster, more agile delivery workflows by providing native support for feature flags directly into your development and delivery process. This feature is currently in `alpha` and we look to mature it and grow it into a truly competitive solution over the next months.

Feature Flags is built with an [Unleash](https://github.com/Unleash/unleash)-compatible API, ensuring interoperability with any other compatible tooling, and taking advantage of the various client libraries available for Unleash.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=feature%20flags&sort=milestone)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Reach out to PM Jason Lenny ([E-Mail](mailto:jason@gitlab.com) / 
[Twitter](https://twitter.com/j4lenn)) if you'd like to provide feedback or ask 
questions about what's coming.

## What's Next & Why

Feature Flags is a very new feature. Our best first large customer is ourselves, so working with our Delivery team to enable the production use of our own feature flags platform is our first target. The first and foremost requirement shared by that team is to implement permissions as per [gitlab-ee#8293](https://gitlab.com/gitlab-org/gitlab-ee/issues/8239), but in order to do this we must first enable per-environment state for feature flags with [gitlab-ee#8621](https://gitlab.com/gitlab-org/gitlab-ee/issues/8621).

Because the category is so new and remains in alpha, there are a lot of obvious features to go after including % rollout ([gitlab-ee#8240](https://gitlab.com/gitlab-org/gitlab-ee/issues/8240)), but dogfooding our own feature is a priority because this will both prove the feature out in an advanced scenario, as well as enable very fast feature feedback loops as we improve over time.

## Competitive Landscape

### LaunchDarkly

LaunchDarkly is a mature feature flagging platform, offering many advanced features. The most glaring gap for anyone wanting to use our alpha feature flags capability is surely that our product currently lacks the ability to implement % rollout, a very common use case for feature flags beyond toggling on/off. We plan to implement this feature via via [gitlab-ee#8240](https://gitlab.com/gitlab-org/gitlab-ee/issues/8240).

There is a detailed LaunchDarkly comparison from when the project was first being conceived [here](https://docs.google.com/spreadsheets/d/1p3QhVvdL7-RCD2pd8mm5a5q38D958a-vwPPWnBT4pmE/edit#gid=0).

### Split.io

For the same reasons as LaunchDarkly, [gitlab-ee#8240](https://gitlab.com/gitlab-org/gitlab-ee/issues/8240) (adding % rollout) must be our next priority to make the feature more valuable to more users.

## Analyst Landscape

We do not yet engage with analysts in the Feature Flags space, so this is N/A for now.

## Top Customer Success/Sales Issue(s)

None yet, but feedback is welcome.

## Top Customer Issue(s)

TBD

## Top Internal Customer Issue(s)

Being able to control permissions on a per-environment basis for feature flags is a key driver of adoption for production use cases. [gitlab-ee#8239](https://gitlab.com/gitlab-org/gitlab-ee/issues/8239) represents our post-MVC vision for implementing basic controls around permissions.

This item was sourced from our own Delivery team, a key early adopter for this new feature.

## Top Vision Item(s)

To push forward the vision for Feature Flags, we need to add % rollout via [gitlab-ee#8240](https://gitlab.com/gitlab-org/gitlab-ee/issues/8240) as the next most obvious basic feature to support normal feature flag workflows.
