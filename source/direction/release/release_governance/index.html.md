---
layout: markdown_page
title: "Category Vision - Release Governance"
---

- TOC
{:toc}

## Release Governance

Release Governance includes features such as deploy-time security controls to ensure only trusted container images are deployed on Kubernetes Engine, and more broadly includes all the assurances and evidence collection that are necessary for you to trust the changes you're delivering. For example, having a strong integration with CI/CD system to ensure artifact chain of custody and traceability all the way to commit, assurance of test completion, quality gates, auditing, and any other governance requirements are enforced in the release.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=release%20governance&sort=milestone)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Reach out to PM Jason Lenny ([E-Mail](mailto:jason@gitlab.com) / 
[Twitter](https://twitter.com/j4lenn)) if you'd like to provide feedback or ask 
questions about what's coming.

## What's Next & Why

Up next is identifying and delivering an MVC for [gitlab-ce#8945](https://gitlab.com/gitlab-org/gitlab-ee/issues/8945), which provides more options for enterprises to lock down the path to production. We'll also be taking a look at binary authorization ([gitlab-ee#7268](https://gitlab.com/gitlab-org/gitlab-ee/issues/7268)) at the same time.

## Competitive Landscape

A key capability of products which securely manage releases is to collect evidence associated with releases in a secure way. [gitlab-ce#56030](https://gitlab.com/gitlab-org/gitlab-ce/issues/56030) introduces a new kind of entity that is part of a release, which contains various kinds of evidence (test results, security scans, etc.) that were collected as part of a release generation.

## Analyst Landscape

The analysts in this space tend to focus a lot right now on existing, more legacy style deployment workflows so changes like [gitlab-ce#44041](https://gitlab.com/gitlab-org/gitlab-ce/issues/44041) (which adds manual approval jobs/gates to the GitLab pipeline) will help us perform better for analysts and the kinds of customers who are approaching release management from a more top-down perspective.

Similarly, integrations with technologies like ServiceNow ([gitlab-ee#8373](https://gitlab.com/gitlab-org/gitlab-ee/issues/8373)) will help GitLab fit in better with larger enterprise governance workflows.

## Top Customer Success/Sales Issue(s)

The CS team sees requests for integration with ServiceNow for change management built in to CD pipelines, as per [gitlab-ee#8373](https://gitlab.com/gitlab-org/gitlab-ee/issues/8373). Also, [gitlab-ee#8429](https://gitlab.com/gitlab-org/gitlab-ee/issues/8429) is an internally requested issue which allows required jobs to be defined externally from the project and enforced at runtime. 

## Top Customer Issue(s)

[gitlab-ce#44041](https://gitlab.com/gitlab-org/gitlab-ce/issues/44041) is the most upvoted item and adds an explicit approval job to handle approvals in release workflows.

There are also two Community Contribution items being worked on that introduce more control in the pipeline:

- [gitlab-ce#53906](https://gitlab.com/gitlab-org/gitlab-ce/issues/53906) introduces an identity API which will allow for Vault integrations to ensure separation of credentials between developers and deployments.
- [gitlab-runner#3699](https://gitlab.com/gitlab-org/gitlab-runner/issues/3699) will allow for injection of variables/steps/stages between the .gitlab-ci.yml and when it is run on the runner. More secure enforcement or variables which should have limited visibility can be injected at this point.

## Top Internal Customer Issue(s)

[gitlab-ce#21583](https://gitlab.com/gitlab-org/gitlab-ce/issues/21583) has been requested by the delivery team / @marin to allow for more secure, locked down access to production-type environments instead of relying on more broad project permissions.

## Top Vision Item(s)

[gitlab-ee#8945](https://gitlab.com/gitlab-org/gitlab-ee/issues/8945) is a large issue that introduces more controls on the pipeline itself, ensuring that assets are handed off between stages in a secure way, that compliance evidence is collected and authenticated with binaries, and overall that the path to production is more secure and auditable than it is today. Very much related is binary authorization ([gitlab-ee#7268](https://gitlab.com/gitlab-org/gitlab-ee/issues/7268)) which provides a secure means to validate deployable containers.

Blackout periods ( [gitlab-ce#51738](https://gitlab.com/gitlab-org/gitlab-ce/issues/51738)) will help compliance teams enforce periods where production needs to remain stable/not change.
