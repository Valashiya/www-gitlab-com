---
layout: markdown_page
title: Product Vision - Fulfillment
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product vision for Fulfillment. Fulfillment manages a variety of categories that are important for GitLab's ongoing success such as [licensing, billing, and telemetry](https://about.gitlab.com/handbook/product/categories/#enablement-department).

If you'd like to discuss this vision directly with the product manager for Fulfillment, 
feel free to reach out to Jeremy Watson via [e-mail](mailto:jwatson@gitlab.com), 
[Twitter](https://twitter.com/gitJeremy), or by [scheduling a video call](https://calendly.com/jeremy_/gitlab-product-chat).

## What's next

#### Immediate priorities
* [Add on runner minutes for GitLab.com](https://gitlab.com/gitlab-org/gitlab-ee/issues/3314)
* [Handling group subscription changes automatically](https://gitlab.com/groups/gitlab-org/-/epics/754)

#### Next up
* [Improved trial experience for GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/377), including [allowing a new user to start a trial immediately](https://gitlab.com/gitlab-org/gitlab-ee/issues/7414).
* [Redesigned renewal flow on customers.gitlab.com](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/375)
* [Support external marketplaces](https://gitlab.com/groups/gitlab-org/-/epics/520)
* [Adjustable true-up frequency](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/347)

## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/#prioritization). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Manage can be viewed [here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3Amanage), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!