---
title: "GitLab's Functional Group Updates - June 13th & June 15th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: company
description: "The Functional Groups at GitLab give an update on what they've been working on"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/group-conversations/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Edge Team

[Presentation slides](https://docs.google.com/presentation/d/1V-4CPO35cEvVQFBuEejnEXcohwFPCFNa4utTfDoK4RU/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/r87mL9GkZZs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Research Team

[Presentation slides](https://docs.google.com/presentation/d/1v3jOmthck8LFeObAWgpAiovEsnYO7kSb2xJxO_kgoII/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/XHPdN8r-77Y" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----


Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
