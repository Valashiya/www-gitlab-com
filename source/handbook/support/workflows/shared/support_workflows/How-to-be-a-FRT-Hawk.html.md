---
layout: markdown_page
title: How to Be a First Response Time Hawk
category: Support Workflows
---

### On this page
{:.no_toc}

- TOC
{:toc}


### What is a First Response Time Hawk?

At GitLab, we want to create the support team of the future. Remote is a great first step, but one of the biggest challenges in any support organization is accountability. How do we maintain accountability while also encouraging flexibility? First Response Time Hawk.

This is a rotating role, that will have someone be "on point" weekly, to make sure our Premium SLA tickets get a response and apporproriately prioritized.

### The Process in Action

1. Every Week on Fridays, Managers will add a First Response Hawk for each region to the Support Week in Review to annouce who will be responsible that week.
1. If you are FRT Hawk, you are responsible for Premium first replies for tickets created from 9am - 5pm in your time zone.
1. You have full authority to call in and ask others for help if volume is high, or you are stumped.
1. You should expect to see new and different things that you are not an expert in. Take the time to dive deep and try and understand the problem.

### Dashboards

You can track your progress and see global results [on this Zendesk Dashboard](https://gitlab.zendesk.com/agent/reporting/analytics/period:0/dashboard:ab7DLgKtesWr)

